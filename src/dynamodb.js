var AWS = require('aws-sdk');
var $$ = require('dom7').default;
var auth = require('./auth.js').default;

AWS.config.update({
    region: "us-west-2",
    endpoint: "https://dynamodb.us-west-2.amazonaws.com",
    accessKeyId: auth.key,
    secretAccessKey: auth.secret
});

var dynamodb = new AWS.DynamoDB();
var docClient = new AWS.DynamoDB.DocumentClient();

function uuid(a) { return a ? (a ^ Math.random() * 16 >> a / 4).toString(16) : ([1e7] + -1e3 + -4e3 + -8e3 + -1e11).replace(/[018]/g, uuid) }

function createItem(params) {
    return new Promise(function (resolve, reject) {
        docClient.put(params, function (err, data) {
            console.log(params);
            if (err) {
                console.log("Unable to add item: " + "\n" + JSON.stringify(err, undefined, 2));
                reject(err);
            } else {
                console.log("PutItem succeeded", params);
                resolve(params);
            }
        });
    });
}

function createUser(userData) {
    console.log(userData);
    var userDataFinal = {
        TableName: "User",
        Item: {
            school: userData.school || "",
            id: uuid(),
            firstName: userData.firstName || "",
            lastName: userData.lastName || "",
            email: userData.email || "",
        },
        ConditionExpression: "attribute_not_exists(id)"
    }
    console.log("adding user...");
    return createItem(userDataFinal);
}

function createEvent(eventData) {
    console.log(eventData);
    var eventDataFinal = {
        TableName: "Event",
        Item: {
            school: eventData.school || "",
            id: uuid(),
            title: eventData.title || "",
            location: eventData.location || "",
            startTime: eventData.startTime || "",
            endTime: eventData.endTime || "",
        },
        ConditionExpression: "attribute_not_exists(id)"
    }
    console.log("adding event...");
    return createItem(eventDataFinal);
}

function deleteUser(school, id) {
    var params = {
        TableName: "User",
        Key: {
            "school": school,
            "id": id
        }
    };
    console.log("deleting user...");
    return new Promise(function (resolve, reject) {
        docClient.delete(params, function (err, data) {
            if (err) {
                console.log("Unable to delete item: " + "\n" + JSON.stringify(err, undefined, 2));
                reject(err);
            } else {
                console.log("DeleteItem succeeded", params);
                resolve(params);
            }
        });
    });
}

function deleteEvent(school, id) {
    var params = {
        TableName: "Event",
        Key: {
            "school": school,
            "id": id
        }
    };
    console.log("deleting event...");
    return new Promise(function (resolve, reject) {
        docClient.delete(params, function (err, data) {
            if (err) {
                console.log("Unable to delete item: " + "\n" + JSON.stringify(err, undefined, 2));
                reject(err);
            } else {
                console.log("DeleteItem succeeded", params);
                resolve(params);
            }
        });
    });
}

function getUser(school, id) {
    var params = {
        TableName: "User",
        Key: {
            "school": school,
            "id": id
        }
    };
    console.log("getting user...");
    return new Promise(function (resolve, reject) {
        docClient.get(params, function (err, data) {
            if (err) {
                console.log("Unable to get user: " + "\n" + JSON.stringify(err, undefined, 2));
                reject(err);
            } else {
                console.log("User found", data);
                resolve(data);
            }
        });
    });
}

function query(params) {
    return new Promise(function (resolve, reject) {
        docClient.query(params, function (err, data) {
            if (err) {
                console.log("Unable to query table: " + "\n" + JSON.stringify(err, undefined, 2));
                reject(err);
            } else {
                console.log("Query complete", data);
                resolve(data);
            }
        });
    });
}

function queryUsers(school) {
    var params = {
        TableName: "User",
        KeyConditionExpression: "#sc = :school",
        ExpressionAttributeNames: {
            "#sc": "school"
        },
        ExpressionAttributeValues: {
            ":school": school
        }
    };
    console.log("querying users...");
    return query(params);
}

function queryUsersByName(school, firstName, lastName) {
    var params = {
        TableName: "User",
        KeyConditionExpression: "#sc = :school",
        FilterExpression: "firstName = :firstName"
            + " AND lastName = :lastName",
        ExpressionAttributeNames: {
            "#sc": "school"
        },
        ExpressionAttributeValues: {
            ":school": school,
            ":firstName": firstName,
            ":lastName": lastName
        }
    };
    console.log("querying users...");
    return query(params);
}

function queryEvents(school) {
    var params = {
        TableName: "Event",
        KeyConditionExpression: "#sc = :school",
        ExpressionAttributeNames: {
            "#sc": "school"
        },
        ExpressionAttributeValues: {
            ":school": school
        }
    };
    console.log("querying events...");
    return query(params);
}

function deleteTable(name) {
    return new Promise(function (resolve, reject) {
        dynamodb.deleteTable({ TableName: name },
            function (err, data) {
                if (err) {
                    console.log("Unable to delete table: " + "\n" + JSON.stringify(err, undefined, 2));
                    reject(err);
                } else {
                    console.log("Table deleted", data);
                    resolve(data);
                }
            });
    });
}

function deleteAllTables() {
    dynamodb.listTables({}, function (err, data) {
        data.TableNames.forEach(function (name) {
            deleteTable(name);
        });
    });
}

function createTable(params) {
    return new Promise(function (resolve, reject) {
        dynamodb.createTable(params, function (err, data) {
            if (err) {
                console.log("Unable to create table: " + "\n" + JSON.stringify(err, undefined, 2));
                reject(err);
            } else {
                console.log("Created table", data);
                resolve(data);
            }
        });
    });
}
function createUserTable() {
    var userTable = {
        TableName: "User",
        KeySchema: [
            { AttributeName: "school", KeyType: "HASH" },
            { AttributeName: "id", KeyType: "RANGE" },
        ],
        AttributeDefinitions: [
            { AttributeName: "school", AttributeType: "S" },
            { AttributeName: "id", AttributeType: "S" },
        ],
        ProvisionedThroughput: {
            ReadCapacityUnits: 5,
            WriteCapacityUnits: 5
        }
    };
    return createTable(userTable);
}
function createEventTable() {
    var eventTable = {
        TableName: "Event",
        KeySchema: [
            { AttributeName: "school", KeyType: "HASH" },
            { AttributeName: "id", KeyType: "RANGE" },
        ],
        AttributeDefinitions: [
            { AttributeName: "school", AttributeType: "S" },
            { AttributeName: "id", AttributeType: "S" },
        ],
        ProvisionedThroughput: {
            ReadCapacityUnits: 5,
            WriteCapacityUnits: 5
        }
    };
    return createTable(eventTable);
}

export {
    createUser, deleteUser, getUser, queryUsers, queryUsersByName,
    createEvent, deleteEvent, queryEvents,
    createUserTable, createEventTable, deleteTable, deleteAllTables
};
