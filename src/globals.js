import moment from "moment";
import Vue from 'vue';

var dayNames = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
var dayNamesShort = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"];

export const globals = new Vue({
    data() {
        return {
            get dayNames() {
                return dayNames;
            },
            get dayNamesShort() {
                return dayNamesShort;
            },
            get now() {
                return moment("2018-10-06 00:00");
            }
        }
    },
    methods: {
        stringToDate(dateString) {
            var dateParts = dateString.split("-");
            var dateObj = new Date(
                Number(dateParts[0]),
                Number(dateParts[1]) - 1,
                Number(dateParts[2])
            );
            return dateObj;
        },
        dateToStamp(dateObj) {
            var dateStamp = dateObj.toJSON().slice(0, 10);
            var minutes = dateObj.getHours() * 60 + dateObj.getMinutes();
            return dateStamp + String(minutes).padStart(4, "0");
        },
        dbTimeToDateObject(time) {
            var dateObj = this.stringToDate(time.date);
            dateObj.setHours(Math.floor(time.min / 60));
            dateObj.setMinutes(time.min % 60);
            return dateObj;
        }
    }
})
