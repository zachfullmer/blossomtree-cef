import testPage from './pages/Test.vue';
import schedulePage from './pages/Schedule.vue';

export default [
  {
    path: '/',
    component: schedulePage,
  },
];
